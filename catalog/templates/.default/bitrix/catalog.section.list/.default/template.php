<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
$this->setFrameMode(true);
?>
<?php if (count($arResult['SECTIONS']) > 0): ?>
   <?php foreach($arResult['SECTIONS'] as $arSection):?>
                    <a href="<?= $arSection["SECTION_PAGE_URL"] ?>" class="product-section">
                        <?= $arSection["NAME"] ?>
                    </a>
<?php endforeach; ?>
<?php endif; ?>